package com.example.commonpom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonPomApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonPomApplication.class, args);
    }

}
