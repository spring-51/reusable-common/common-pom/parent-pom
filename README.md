# Parent (Common) POM project

## Steps to create parent pom
```
s1 - Create a maven project(using archetype selection or via spring starter)
s2 - Change packaging type to pom
     i.e. <packaging>pom</packaging>

s3 - Define common depencies , properties, plugin etc
s4 - mvn clean install - to create local parent pom 
s5 - Push it to Cntral repo(maven or any thing else)  
```

## Steps to integrate this parent pom in child projects

```
s1 - Add/Modify <parent> of child project pom.xml as
  <parent>
        <groupId>{group-id-of-parent-pom}</groupId>
        <artifactId>{artifact-id-of-parent-pom}</artifactId>
        <version>{version-of-parent-pom}</version>
    </parent>
    
refer - 
```